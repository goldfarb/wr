const prefixW = "https://darebee.com/workouts/"
const prefixImg = "https://darebee.com/images/workouts/"

let arm = ["super-strength-workout","strength-and-power-workout","bicep-and-triceps-workout","catalyst-workout","trim-and-tone-arms-workout","upperbody-blast-workout","smiter-workout","home-upperbody-tone-workout","serious-lifts-workout"]

let leg = ["best-thing-workout","grind-workout","better-tomorrow-workout","cellulite-workout","lean-legs-workout","friday-workout"]

let abs = ["solid-core-workout","two-minute-plank-workout","core-conditioning-workout"]

let full = ["best-thing-workout","no-regrets-workout","druid-workout","take-off-workout"]

let suffix = "";

function choose(a) {
	console.log(a)
	switch(a) {
		case "arm":
			suffix = arm[Math.floor(Math.random() * arm.length)];
			break;
		case "leg":
			suffix = leg[Math.floor(Math.random() * leg.length)]
			break;
		case "abs":
			suffix = abs[Math.floor(Math.random() * abs.length)]
			break;
		case "full":
			suffix = full[Math.floor(Math.random() * full.length)]
			break;
	}
	
	console.log("\t"+suffix);
    let infoBox = document.getElementById("inputResults");
    let button = document.getElementById(a);
    button.style.backgroundColor = 'white';

    page = prefixImg + suffix + ".jpg";
    
    window.open(page);
}

function done(a) {
	console.log(a)	
	
    let infoBox = document.getElementById("inputResults");
    let button = document.getElementById(a);
    button.style.backgroundColor = 'white';

}